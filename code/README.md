####First step:
get copy of nano-network example
####Second step:
understand configtx.yaml and crypto-config.yaml
####Third:
configure 

generate crypto config on one node and then transfer this to the other ones.
- because certificates and keys would be different if it was generated on different nodes.

commands: after copy

    cd hyperledger-fabric/winner-test-network/
    sudo apt install dos2unix
    chmod +x *.sh
    dos2unix *

fetch genesis block to join channel:
    
    peer channel fetch 0 -c mychannel -o 192.168.178.251:6050 --tls --cafile <PATH>


but get back an ''mychannel_0.block''

    peer channel fetch 0 channel-artifacts/mychannel.block -c mychannel -o 192.168.178.251:6050 --tls --cafile <PATH>

Package and install the chaincode on peer1:

    peer lifecycle chaincode package basic.tar.gz --path ../asset-transfer-basic/chaincode-go --lang golang --label basic_1
    peer lifecycle chaincode install basic.tar.gz

The chaincode install may take a minute since the fabric-ccenv chaincode builder docker image will be downloaded if not already available on your machine. Copy the returned chaincode package ID into an environment variable for use in subsequent commands (your ID may be different):

    export CC_PACKAGE_ID=basic_1:faaa38f2fc913c8344986a7d1617d21f6c97bc8d85ee0a489c90020cd57af4a5

Approve and commit the chaincode (only a single approver is required based on the lifecycle endorsement policy of any organization):

    peer lifecycle chaincode approveformyorg -o 192.168.178.251:6050 --channelID mychannel --name basic --version 1 --package-id $CC_PACKAGE_ID --sequence 1 --tls --cafile ${PWD}/crypto-config/ordererOrganizations/winner-testnet.com/orderers/orderer1_hl-node4.winner-testnet.com/tls/ca.crt
    peer lifecycle chaincode commit -o 192.168.178.251:6050 --channelID mychannel --name basic --version 1 --sequence 1 --tls --cafile "${PWD}"/crypto-config/ordererOrganizations/winner-testnet.com/orderers/orderer1_hl-node4.winner-testnet.com/tls/ca.crt

Invoke the chaincode to create an asset (only a single endorser is required based on the default endorsement policy of any organization). Then query the asset, update it, and query again to see the resulting asset changes on the ledger.

    peer chaincode invoke -o 192.168.178.251:6050 -C mychannel -n basic -c '{"Args":["CreateAsset","1","blue","35","tom","1000"]}' --tls --cafile "${PWD}"/crypto-config/ordererOrganizations/winner-testnet.com/orderers/orderer1_hl-node4.winner-testnet.com/tls/ca.crt

    peer chaincode query -C mychannel -n basic -c '{"Args":["ReadAsset","1"]}'

    peer chaincode invoke -o 192.168.178.251:6050 -C mychannel -n basic -c '{"Args":["UpdateAsset","1","blue","35","jerry","1000"]}' --tls --cafile "${PWD}"/crypto-config/ordererOrganizations/winner-testnet.com/orderers/orderer1_hl-node4.winner-testnet.com/tls/ca.crt

    peer chaincode query -C mychannel -n basic -c '{"Args":["ReadAsset","1"]}'


org4

    cryptogen generate --config=org4-crypto-config.yaml --output="crypto-config/peerOrganisations/"


    configtxgen -configPath ./ -printOrg Org4MSP > crypto-config/peerOrganizations/org4.winner-testnet.com/org4.json