package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"math"
	"strconv"
	"strings"
)

// E_Meter describes the main object being updated to achive a protocol of metering values for invoice
type E_Meter struct {
	ObjectType         string  `json:"objectType"`
	E_Meter_ID         string  `json:"e_meter_ID"`
	Owner_ID           string  `json:"owner_ID"`
	Flat_ID            string  `json:"flat_ID"`
	EnergyRessource_ID string  `json:"energyRessource_ID"`
	Meter_value        float64 `json:"meter_value"`
	Last_meter_value   float64 `json:"last_meter_value"`
	Diff_meter_value   float64 `json:"diff_meter_value"`
	Unit               string  `json:"unit"`
	UpdateTS           int64   `json:"updateTS"`
	IsActive           bool    `json:"isActive"`
}

// QueryResult structure used for handling result of history-query
type QueryE_meterHistoryResult struct {
	TxId      string `json:"TxId"`
	Key       string `json:"Key"`
	Timestamp int64  `json:"timestamp"`
	IsDelete  bool   `json:"isDelete"`
	Record    *E_Meter
}

type EnergyRessource struct {
	ObjectType  string `json:"objectType"`
	BuildingID  string `json:"buildingID"`
	External_ID string `json:"ext_ID"`
	Type        string `json:"ressourceType"`
}

// CreateE_Meter adds a new flat to the world state with given details
func (s *SmartContract) CreateE_Meter(ctx contractapi.TransactionContextInterface, e_meter_ID string, owner_ID string, flat_ID string, energyRessource_ID string, meter_value string, unit string, updateTS string) error {
	temp, err := s.QueryE_Meter(ctx, e_meter_ID)
	if temp != nil && err == nil && temp.IsActive == true {
		return fmt.Errorf("E_meter with this ID already exist.")
	}

	// parse String values
	_meter_value, err := strconv.ParseFloat(meter_value, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_updateTS, err := strconv.ParseInt(updateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	e_meter := E_Meter{
		ObjectType:         "E_Meter",
		E_Meter_ID:         e_meter_ID,
		Owner_ID:           owner_ID,
		Flat_ID:            flat_ID,
		EnergyRessource_ID: energyRessource_ID,
		Meter_value:        _meter_value,
		Last_meter_value:   _meter_value,
		Diff_meter_value:   0,
		Unit:               unit,
		UpdateTS:           _updateTS,
		IsActive:           true,
	}

	e_meterAsBytes, _ := json.Marshal(e_meter)

	return ctx.GetStub().PutState(e_meter_ID, e_meterAsBytes)
}

// QueryE_Meter returns the E_Meter stored in the world state with given id
func (s *SmartContract) QueryE_Meter(ctx contractapi.TransactionContextInterface, e_meter_ID string) (*E_Meter, error) {
	e_meterAsBytes, err := ctx.GetStub().GetState(e_meter_ID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if e_meterAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", e_meter_ID)
	}

	e_meter := new(E_Meter)
	_ = json.Unmarshal(e_meterAsBytes, e_meter)

	return e_meter, nil
}

// QueryE_Meter returns the E_Meter stored in the world state with given id
func (s *SmartContract) ChangeE_Meter(ctx contractapi.TransactionContextInterface, oldE_meter_ID string, oldMeter_value string, newE_meter_ID string, newE_meter_ownerID string, newMeter_value string, updateTS string) error {

	//get old E_Meter and run last update
	oldE_meter, err := s.QueryE_Meter(ctx, oldE_meter_ID)
	if err != nil {
		return fmt.Errorf("Failed to read from world state. %s", err.Error())
	}
	err = s.UpdateE_MeterValue(ctx, oldE_meter_ID, oldMeter_value, updateTS)
	if err != nil {
		return err
	}

	//is new E_Meter already in world State?
	newE_meter, err := s.QueryE_Meter(ctx, newE_meter_ID)
	if newE_meter != nil && newE_meter.IsActive == false {
		err = s.ReactivateE_Meter(ctx, newE_meter_ID, newMeter_value, newE_meter_ownerID, oldE_meter.Flat_ID, oldE_meter.EnergyRessource_ID, updateTS)
	} else if strings.Contains(err.Error(), "does not exist") {
		err = s.CreateE_Meter(ctx, newE_meter_ID, newE_meter_ownerID, oldE_meter.Flat_ID, oldE_meter.EnergyRessource_ID, newMeter_value, oldE_meter.Unit, updateTS)
		if err != nil {
			return fmt.Errorf("Failed to create new E_meter: %s . %s", newE_meter_ID, err.Error())
		}
	}
	if err != nil {
		return fmt.Errorf("Failed get world state for new E_meter: %s . %s", newE_meter_ID, err.Error())
	}

	//change E_Meter in contract & deactivate old one
	err = s.ChangeE_MeterInEnergyContract(ctx, oldE_meter_ID, newE_meter_ID, updateTS)
	if err != nil {
		return fmt.Errorf("Failed to exchange E_Meter %s in EnergyContract. %s", newE_meter_ID, err.Error())
	}
	return s.DeactivateE_Meter(ctx, oldE_meter_ID, updateTS)
}

func (s *SmartContract) QueryE_MeterBetweenTS(ctx contractapi.TransactionContextInterface, e_meter_ID string, startTS string, endTS string) ([]QueryE_meterHistoryResult, error) {
	resultsIterator, err := ctx.GetStub().GetHistoryForKey(e_meter_ID)

	results := []QueryE_meterHistoryResult{}
	if err != nil {
		return results, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}
	defer resultsIterator.Close()

	// parse String values
	_startTS, err := strconv.ParseInt(startTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_endTS, err := strconv.ParseInt(endTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return results, fmt.Errorf("Failed to read next. %s", err.Error())
		}

		e_meter := new(E_Meter)
		_ = json.Unmarshal(response.Value, e_meter)

		if e_meter.UpdateTS <= _startTS {
			continue
		} else if e_meter.UpdateTS > _endTS { //also use >= to get real results or let it be to get no cloned values for invoice?
			continue
		}
		result := QueryE_meterHistoryResult{Key: e_meter_ID, Record: e_meter, TxId: response.TxId, Timestamp: response.Timestamp.Seconds, IsDelete: response.IsDelete}
		results = append(results, result)
	}
	return results, nil
}

// UpdateFlatMeterValue updates the Meter-Values field of flat with given id in world state
func (s *SmartContract) UpdateE_MeterValue(ctx contractapi.TransactionContextInterface, e_meter_ID string, newMeterValue string, newUpdateTS string) error {
	e_meter, err := s.QueryE_Meter(ctx, e_meter_ID)

	if err != nil {
		return err
	}
	if !e_meter.IsActive {
		return fmt.Errorf("%s is deactivated, please check your data or reactivate if valid", e_meter.E_Meter_ID)
	}

	// parse String values
	_newMeterValue, err := strconv.ParseFloat(newMeterValue, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_newUpdateTS, err := strconv.ParseInt(newUpdateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	e_meter.Last_meter_value = e_meter.Meter_value
	e_meter.Meter_value = _newMeterValue
	e_meter.Diff_meter_value = math.Abs(_newMeterValue - e_meter.Last_meter_value)
	e_meter.UpdateTS = _newUpdateTS

	e_meterAsBytes, _ := json.Marshal(e_meter)
	return ctx.GetStub().PutState(e_meter_ID, e_meterAsBytes)
}

// ReactivateE_Meter reactivates an E_Meter was deactivated before and already existing in worldState
func (s *SmartContract) ReactivateE_Meter(ctx contractapi.TransactionContextInterface, e_meter_ID string, e_meter_value string, owner_ID string, flatID string, energyRessoury_ID string, newUpdateTS string) error {
	e_meter, err := s.QueryE_Meter(ctx, e_meter_ID)
	if err != nil {
		return err
	}

	// parse String values
	_e_meter_value, err := strconv.ParseFloat(e_meter_value, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_newUpdateTS, err := strconv.ParseInt(newUpdateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	e_meter.Owner_ID = owner_ID
	e_meter.Flat_ID = flatID
	e_meter.EnergyRessource_ID = energyRessoury_ID
	e_meter.Last_meter_value = _e_meter_value
	e_meter.Meter_value = _e_meter_value
	e_meter.Diff_meter_value = 0
	e_meter.IsActive = true
	e_meter.UpdateTS = _newUpdateTS

	e_meterAsBytes, _ := json.Marshal(e_meter)
	return ctx.GetStub().PutState(e_meter_ID, e_meterAsBytes)
}

// DeactivateE_Meter deactivates an E_Meter existing in worldState
func (s *SmartContract) DeactivateE_Meter(ctx contractapi.TransactionContextInterface, e_meter_ID string, newUpdateTS string) error {
	e_meter, err := s.QueryE_Meter(ctx, e_meter_ID)
	if err != nil {
		return err
	}

	// parse String values
	_newUpdateTS, err := strconv.ParseInt(newUpdateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	e_meter.IsActive = false
	e_meter.UpdateTS = _newUpdateTS

	e_meterAsBytes, _ := json.Marshal(e_meter)
	return ctx.GetStub().PutState(e_meter_ID, e_meterAsBytes)
}
