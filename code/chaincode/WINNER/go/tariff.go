package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"strconv"
)

// EnergyPrice describes basic details of what makes up a EnergyPrice for price-tracking
type EnergyPrice struct {
	ObjectType    string  `json:"objectType"`
	Price         float64 `json:"price"`
	Currency      string  `json:"currency"`
	StartTS       int64   `json:"startTS"`
	EndTS         int64   `json:"endTS"`
	Owner         string  `json:"owner"`
	UpdateTS      int64   `json:"updateTS"`
	BuildingPrice float64 `json:"buildingPrice"`
	Metadata      string  `json:"metadata"`
}

// EnergyPriceQueryResult structure used for handling result of query
type EnergyPriceQueryResult struct {
	Key    string `json:"key"`
	Record *EnergyPrice
}

// EnergyPriceQueryResult structure used for handling result of history-query
type EnergyPriceQueryHistoryResult struct {
	TxId      string `json:"TxId"`
	Key       string `json:"key"`
	Timestamp int64  `json:"timestamp"`
	IsDelete  bool   `json:"isDelete"`
	Record    *EnergyPrice
}

type TariffModel struct {
	ObjectType  string `json:"objectType"`
	Name        string `json:"name"`
	Metadata    string `json:"metadata"`
	External_ID string `json:"external_ID"`
	Price_ID    string `json:"price_ID"`
}

type Invoice struct {
	ObjectType         string                          `json:"objectType"`
	ExternalInvoice_ID string                          `json:"ext_invoice_ID"`
	Contract_ID        string                          `json:"contract_ID"`
	StartTS            int64                           `json:"startTS"`
	EndTS              int64                           `json:"endTS"`
	EnergyAmount       []float64                       `json:"energy_amount"`
	EnergyPrice        []EnergyPriceQueryHistoryResult `json:"energy_price"`
	UpdateTS           int64                           `json:"updateTS"`
	PaymentState       string                          `json:"payment_state"`
	PaymentAmount      string                          `json:"payment_amount"`
}

// CreateEnergyPrice adds a new EnergyPriceID to the world state with given details
func (s *SmartContract) CreateEnergyPrice(ctx contractapi.TransactionContextInterface, priceID string, energyPrice string, startTS string, updateTS string, owner string, buildingPrice string, currency string, metadata string) error {
	temp, err := s.QueryEnergyPrice(ctx, priceID)
	if temp != nil && err == nil {
		return fmt.Errorf("EnergyPrice with this ID already exist.")
	}

	// parse String values
	_startTS, err := strconv.ParseInt(startTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_updateTS, err := strconv.ParseInt(updateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_buildingPrice, err := strconv.ParseFloat(buildingPrice, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_energyPrice, err := strconv.ParseFloat(energyPrice, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	price := EnergyPrice{
		ObjectType:    "EnergyPrice",
		Price:         _energyPrice,
		StartTS:       _startTS,
		BuildingPrice: _buildingPrice,
		UpdateTS:      _updateTS,
		Owner:         owner,
		Metadata:      metadata,
		Currency:      currency,
	}

	priceAsBytes, _ := json.Marshal(price)

	return ctx.GetStub().PutState(priceID, priceAsBytes)
}

// QueryEnergyPrice returns the energy-price stored in the world state with given id
func (s *SmartContract) QueryEnergyPrice(ctx contractapi.TransactionContextInterface, priceID string) (*EnergyPrice, error) {
	priceAsBytes, err := ctx.GetStub().GetState(priceID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if priceAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", priceID)
	}

	price := new(EnergyPrice)
	_ = json.Unmarshal(priceAsBytes, price)

	return price, nil
}

// QueryEnergyPriceHistory returns the flat history for a given id
func (s *SmartContract) QueryEnergyPriceHistory(ctx contractapi.TransactionContextInterface, priceID string) ([]EnergyPriceQueryHistoryResult, error) {
	resultsIterator, err := ctx.GetStub().GetHistoryForKey(priceID)

	results := []EnergyPriceQueryHistoryResult{}

	if err != nil {
		return results, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}
	defer resultsIterator.Close()

	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return results, fmt.Errorf("Failed to read next. %s", err.Error())
		}

		price := new(EnergyPrice)
		_ = json.Unmarshal(response.Value, price)

		result := EnergyPriceQueryHistoryResult{Key: priceID, Record: price, TxId: response.TxId, Timestamp: response.Timestamp.Seconds, IsDelete: response.IsDelete}
		results = append(results, result)
	}

	return results, nil
}

func (s *SmartContract) QueryEnergyPriceHistoryBetweenTS(ctx contractapi.TransactionContextInterface, priceID string, startTS string, endTS string) ([]EnergyPriceQueryHistoryResult, error) {
	resultsIterator, err := ctx.GetStub().GetHistoryForKey(priceID)

	results := []EnergyPriceQueryHistoryResult{}
	if err != nil {
		return results, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}
	defer resultsIterator.Close()

	// parse String values
	_startTS, err := strconv.ParseInt(startTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_endTS, err := strconv.ParseInt(endTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return results, fmt.Errorf("Failed to read next. %s", err.Error())
		}

		price := new(EnergyPrice)
		_ = json.Unmarshal(response.Value, price)

		if price.StartTS < _startTS {
			continue
		} else if price.EndTS > _endTS {
			continue
		}

		result := EnergyPriceQueryHistoryResult{Key: priceID, Record: price, TxId: response.TxId, Timestamp: response.Timestamp.Seconds, IsDelete: response.IsDelete}
		results = append(results, result)

	}

	return results, nil
}

// UpdateEnergyPriceValue updates the Price field of EnergyPrice with given id in world state
func (s *SmartContract) UpdateEnergyPriceValue(ctx contractapi.TransactionContextInterface, priceID string, newPriceValue string, newUpdateTS string, newPriceStartTS string, newBuildingPrice string) error {
	price, err := s.QueryEnergyPrice(ctx, priceID)
	if err != nil {
		return err
	}

	// parse String values
	_newUpdateTS, err := strconv.ParseInt(newUpdateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_newPriceStartTS, err := strconv.ParseInt(newPriceStartTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_newBuildingPrice, err := strconv.ParseFloat(newBuildingPrice, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_newPriceValue, err := strconv.ParseFloat(newPriceValue, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	price.Price = _newPriceValue
	price.UpdateTS = _newUpdateTS
	price.StartTS = _newPriceStartTS
	price.BuildingPrice = _newBuildingPrice

	priceAsBytes, _ := json.Marshal(price)
	return ctx.GetStub().PutState(priceID, priceAsBytes)
}

// CreateTariffModel adds a new TariffModel to the world state with given details
func (s *SmartContract) CreateTariffModel(ctx contractapi.TransactionContextInterface, tariffID string, name string, externalID string, priceID string, metadata string) error {
	temp, err := s.QueryTariffModel(ctx, tariffID)

	if temp != nil && err == nil {
		return fmt.Errorf("TariffModel with this ID already exist.")
	}
	tariff := TariffModel{
		ObjectType:  "TariffModel",
		Name:        name,
		External_ID: externalID,
		Metadata:    metadata,
		Price_ID:    priceID,
	}

	tariffAsBytes, _ := json.Marshal(tariff)

	return ctx.GetStub().PutState(tariffID, tariffAsBytes)
}

// QueryTariffModel returns the TariffModel stored in the world state with given id
func (s *SmartContract) QueryTariffModel(ctx contractapi.TransactionContextInterface, tariffID string) (*TariffModel, error) {
	tariffAsBytes, err := ctx.GetStub().GetState(tariffID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if tariffAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", tariffID)
	}

	tariffModel := new(TariffModel)
	_ = json.Unmarshal(tariffAsBytes, tariffModel)

	if tariffModel.ObjectType != "TariffModel" {
		return nil, fmt.Errorf("Given ID does not belong to an TariffModel. %s", err.Error())
	}
	return tariffModel, nil
}

// CreateInvoice adds a new Invoice to the world state with given details
func (s *SmartContract) CreateInvoice(ctx contractapi.TransactionContextInterface, invoiceID string, contractID string, externalInvoiceID string, startTS string, endTS string) error {
	temp, err := s.QueryInvoice(ctx, invoiceID)
	if temp != nil && err == nil {
		return fmt.Errorf("Invoice with this ID already exist.")
	}

	// parse String values
	_startTS, err := strconv.ParseInt(startTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_endTS, err := strconv.ParseInt(endTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	energyContract := new(EnergyContract)
	energyContract, err = s.QueryEnergyContract(ctx, contractID)
	if err != nil {
		return fmt.Errorf("Failed to read contract from world state. %s", err.Error())
	}

	e_MeterContractResults := []QueryE_MeterContractResult{}
	e_MeterContractResults, err = s.QueryAllE_MeterForContractBetweenTS(ctx, contractID, startTS, endTS)
	tariffModel := new(TariffModel)
	tariffModel, err = s.QueryTariffModel(ctx, energyContract.TariffModel_ID)
	if err != nil {
		return fmt.Errorf("Failed to read tariff from world state. %s", err.Error())
	}

	e_meterResults := []QueryE_meterHistoryResult{}
	for i := 0; i < len(e_MeterContractResults); i++ {
		e_meter := []QueryE_meterHistoryResult{}
		e_meter, err = s.QueryE_MeterBetweenTS(ctx, e_MeterContractResults[i].E_Meter_ID, fmt.Sprintf("%d", e_MeterContractResults[i].StartTS), fmt.Sprintf("%d", e_MeterContractResults[i].EndTS))
		if err != nil {
			return fmt.Errorf("Failed to get history for E_Meter. %s", err.Error())
		}
		e_meterResults = append(e_meterResults, e_meter...)
	}

	energyPriceResults := []EnergyPriceQueryHistoryResult{}
	energyPriceResults, err = s.QueryEnergyPriceHistoryBetweenTS(ctx, tariffModel.Price_ID, startTS, endTS)
	if err != nil {
		return fmt.Errorf("Failed to get history for energyPrice. %s", err.Error())
	}

	var amountOfEnergy []float64
	var priceOfEnergy []float64
	var sum = 0.0
	var counterE_Meter = 0
	var lastprice = -1.0

	for i := 0; i < len(energyPriceResults); i++ { //TODO
		var energyForPrice = 0.0
		if lastprice < 0 {
			lastprice = energyPriceResults[i].Record.Price
		}
		for j := counterE_Meter; j < len(e_meterResults); j++ {

			if e_meterResults[j].Record.UpdateTS < energyPriceResults[i].Record.StartTS {
				amountOfEnergy = append(amountOfEnergy, energyForPrice)
				priceOfEnergy = append(priceOfEnergy, lastprice)
				sum += energyForPrice * lastprice
				counterE_Meter = j
				break
			} else {
				energyForPrice += e_meterResults[j].Record.Diff_meter_value
				lastprice = energyPriceResults[i].Record.Price
			}

		}
	}

	invoice := Invoice{
		ObjectType:         "Invoice",
		Contract_ID:        contractID,
		ExternalInvoice_ID: externalInvoiceID,
		StartTS:            _startTS,
		EndTS:              _endTS,
		EnergyPrice:        energyPriceResults,
		EnergyAmount:       amountOfEnergy,
		PaymentAmount:      fmt.Sprintf("%.2f", sum) + "€",
	}

	invoiceAsBytes, _ := json.Marshal(invoice)

	return ctx.GetStub().PutState(invoiceID, invoiceAsBytes)
}

// QueryInvoice returns the Invoice stored in the world state with given id
func (s *SmartContract) QueryInvoice(ctx contractapi.TransactionContextInterface, invoiceID string) (*Invoice, error) {
	invoiceAsBytes, err := ctx.GetStub().GetState(invoiceID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if invoiceAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", invoiceID)
	}

	invoice := new(Invoice)
	_ = json.Unmarshal(invoiceAsBytes, invoice)

	if invoice.ObjectType != "Invoice" {
		return nil, fmt.Errorf("Given ID does not belong to an Invoice. %s", err.Error())
	}
	return invoice, nil
}
