package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"strconv"
	"time"
)

// SmartContract provides functions for managing
type SmartContract struct {
	contractapi.Contract
}

// QueryResult structure used for handling result of history-query
type QueryEnergyContractHistoryResult struct {
	TxId      string `json:"TxId"`
	Key       string `json:"Key"`
	Timestamp int64  `json:"timestamp"`
	IsDelete  bool   `json:"isDelete"`
	Record    *EnergyContract
}

// QueryResult structure used for handling result of query
type QueryEnergyContractResult struct {
	Key    string `json:"Key"`
	Record *EnergyContract
}

// QueryResult of E_Meters active in Contract
type QueryE_MeterContractResult struct {
	Key        string `json:"Key"`
	EndTS      int64  `json:"endTS"`
	StartTS    int64  `json:"startTS"`
	E_Meter_ID string `json:"e_Meter_ID"`
}

// QueryResult structure used for handling result of query
type QueryRentalContractResult struct {
	Key    string `json:"Key"`
	Record *RentalContract
}

type EnergyContract struct {
	ObjectType     string `json:"objectType"`
	Contract_ID    string `json:"contract_ID"` //fraglich ob notwendig
	Contractor_ID  string `json:"contractor_ID"`
	Customer_ID    string `json:"customer_ID"`
	StartTS        int64  `json:"startTS"`
	EndTS          int64  `json:"endTS"`
	SigningTS      int64  `json:"signingTS"`
	IsSigned       bool   `json:"isSigned"`
	IsDiscontinued bool   `json:"isDiscontinued"`
	E_meter_ID     string `json:"e_meter_ID"`
	TariffModel_ID string `json:"tariffModel_ID"`
	UpdateTS       int64  `json:"updateTS"`
}

type RentalContract struct {
	ObjectType     string `json:"objectType"`
	Contract_ID    string `json:"contract_ID"` //fraglich ob notwendig
	Contractor_ID  string `json:"contractor_ID"`
	Customer_ID    string `json:"customer_ID"`
	StartTS        int64  `json:"startTS"`
	EndTS          int64  `json:"endTS"`
	SigningTS      int64  `json:"signingTS"`
	IsSigned       bool   `json:"isSigned"`
	IsDiscontinued bool   `json:"isDiscontinued"`
	Flat_ID        string `json:"flat_ID"`
	UpdateTS       int64  `json:"updateTS"`
}

type Tenant struct {
	Forename         string `json:"forename"`
	Name             string `json:"Name"`
	ExternalTenantID string `json:"ExternalTenantID"`
}

// CreateEnergyContract adds a new EnergyContract to the world state with given details
func (s *SmartContract) CreateEnergyContract(ctx contractapi.TransactionContextInterface, contractID string, contractorID string, customerID string, startTS string, endTS string, signingTS string, isSigned string, isDiscontinued string, e_meterID string, tariffModel string) error {
	temp, err := s.QueryEnergyContract(ctx, contractID)

	// parse String values
	_isSigned, err := strconv.ParseBool(isSigned)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_isDiscontinued, err := strconv.ParseBool(isDiscontinued)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_startTS, err := strconv.ParseInt(startTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_endTS, err := strconv.ParseInt(endTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_signingTS, err := strconv.ParseInt(signingTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	if temp != nil && err == nil {
		return fmt.Errorf("EnergyContract with this ID already exist.")
	}
	energyContract := EnergyContract{
		ObjectType:     "EnergyContract",
		Contractor_ID:  contractorID,
		Customer_ID:    customerID,
		StartTS:        _startTS,
		EndTS:          _endTS,
		SigningTS:      _signingTS,
		IsSigned:       _isSigned,
		IsDiscontinued: _isDiscontinued,
		E_meter_ID:     e_meterID,
		TariffModel_ID: tariffModel,
		UpdateTS:       time.Now().Unix(),
	}
	energyContractAsBytes, _ := json.Marshal(energyContract)

	return ctx.GetStub().PutState(contractID, energyContractAsBytes)
}

// ChangeE_MeterInEnergyContract exchange E_Meter in active Energy Contract in worldState
func (s *SmartContract) ChangeE_MeterInEnergyContract(ctx contractapi.TransactionContextInterface, oldE_MeterID string, newE_MeterID string, newUpdateTS string) error {
	energyContracts, err := s.QueryAllEnergyContractsForE_Meter(ctx, oldE_MeterID)

	if err != nil {
		return fmt.Errorf("Error occured when trying to get acive contract for E_Meter")
	}

	if len(energyContracts) > 1 {
		return fmt.Errorf("There are more than 1 active contract for this E_Meter")
	} else if len(energyContracts) == 0 {
		return nil
	}

	// parse String values
	_newUpdateTS, err := strconv.ParseInt(newUpdateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	energyContract := energyContracts[0].Record
	energyContract.UpdateTS = _newUpdateTS
	energyContract.E_meter_ID = newE_MeterID

	energyContractAsBytes, _ := json.Marshal(energyContract)
	return ctx.GetStub().PutState(energyContract.Contract_ID, energyContractAsBytes)
}

// cancelEnergyContract set EnergyContract in state "canceled" in worldState
func (s *SmartContract) cancelEnergyContract(ctx contractapi.TransactionContextInterface, contractID string, endTS string, newUpdateTS string) error {
	energyContract, err := s.QueryEnergyContract(ctx, contractID)

	if err != nil {
		return err
	}

	// parse String values
	_newUpdateTS, err := strconv.ParseInt(newUpdateTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_endTS, err := strconv.ParseInt(endTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	energyContract.UpdateTS = _newUpdateTS
	energyContract.IsDiscontinued = true
	energyContract.EndTS = _endTS

	energyContractAsBytes, _ := json.Marshal(energyContract)
	return ctx.GetStub().PutState(contractID, energyContractAsBytes)
}

// QueryEnergyContract returns the EnergyContract stored in the world state with given id
func (s *SmartContract) QueryEnergyContract(ctx contractapi.TransactionContextInterface, energyContractID string) (*EnergyContract, error) {
	energyContractAsBytes, err := ctx.GetStub().GetState(energyContractID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if energyContractAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", energyContractID)
	}

	energyContract := new(EnergyContract)
	_ = json.Unmarshal(energyContractAsBytes, energyContract)

	return energyContract, nil
}

// QueryEnergyContract returns the history for an EnergyContract with given id
func (s *SmartContract) QueryEnergyContractBetweenTS(ctx contractapi.TransactionContextInterface, contractID string, searchStartTS string, searchEndTS string) ([]QueryEnergyContractHistoryResult, error) {
	resultsIterator, err := ctx.GetStub().GetHistoryForKey(contractID)

	results := []QueryEnergyContractHistoryResult{}
	if err != nil {
		return results, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	// parse String values
	_searchStartTS, err := strconv.ParseInt(searchStartTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_searchEndTS, err := strconv.ParseInt(searchEndTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	defer resultsIterator.Close()

	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return results, fmt.Errorf("Failed to read next. %s", err.Error())
		}

		energyContract := new(EnergyContract)
		_ = json.Unmarshal(response.Value, energyContract)

		if energyContract.UpdateTS <= _searchStartTS {
			continue // TODO: could be a break cause QueryHistoryResult is ordered. optimize performance for near by timerange
		} else if energyContract.UpdateTS > _searchEndTS { //also use >= to get real results or let it be to get no cloned values for invoice?
			continue
		}
		result := QueryEnergyContractHistoryResult{Key: contractID, Record: energyContract, TxId: response.TxId, Timestamp: response.Timestamp.Seconds, IsDelete: response.IsDelete}
		results = append(results, result)
	}
	return results, nil
}

// QueryAllEnergyContracts returns all EnergyContracts found in world state
func (s *SmartContract) QueryAllEnergyContacts(ctx contractapi.TransactionContextInterface) ([]QueryEnergyContractResult, error) {
	objectType := "EnergyContract"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryEnergyContractResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		energyContract := new(EnergyContract)
		_ = json.Unmarshal(queryResponse.Value, energyContract)

		queryResult := QueryEnergyContractResult{Key: queryResponse.Key, Record: energyContract}
		results = append(results, queryResult)
	}

	return results, nil
}

// QueryAllE_MeterForContractBetweenTS returns all EnergyContracts for a specific E_Meter found in world state
func (s *SmartContract) QueryAllE_MeterForContractBetweenTS(ctx contractapi.TransactionContextInterface, contractID string, searchStartTS string, searchEndTS string) ([]QueryE_MeterContractResult, error) {
	resultsIterator, err := ctx.GetStub().GetHistoryForKey(contractID)

	results := []QueryE_MeterContractResult{}
	if err != nil {
		return results, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}
	defer resultsIterator.Close()

	// parse String values
	_searchStartTS, err := strconv.ParseInt(searchStartTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_searchEndTS, err := strconv.ParseInt(searchEndTS, 10, 64)
	if err != nil {
		return results, fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	var e_meterID string
	var endTS = _searchEndTS
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return results, fmt.Errorf("Failed to read next. %s", err.Error())
		}

		energyContract := new(EnergyContract)
		_ = json.Unmarshal(response.Value, energyContract)

		if e_meterID == "" {
			e_meterID = energyContract.E_meter_ID
		}

		if energyContract.UpdateTS <= _searchStartTS {
			break // TODO: could be a break cause QueryHistoryResult is ordered. optimize performance for near by timerange
		} else if energyContract.UpdateTS > _searchEndTS { //also use >= to get real results or let it be to get no cloned values for invoice?
			continue
		}

		if e_meterID != energyContract.E_meter_ID {
			emeterChange := QueryE_MeterContractResult{Key: contractID, EndTS: endTS, StartTS: energyContract.UpdateTS, E_Meter_ID: e_meterID}
			results = append(results, emeterChange)
			e_meterID = energyContract.E_meter_ID
			endTS = energyContract.UpdateTS
		}
	}
	emeterChange := QueryE_MeterContractResult{Key: contractID, EndTS: endTS, StartTS: _searchStartTS, E_Meter_ID: e_meterID}
	results = append(results, emeterChange)
	return results, nil
}

// QueryAllEnergyContractsForE_Meter returns all EnergyContracts for a specific E_Meter found in world state
func (s *SmartContract) QueryAllEnergyContractsForE_Meter(ctx contractapi.TransactionContextInterface, e_meterID string) ([]QueryEnergyContractResult, error) {
	objectType := "EnergyContract"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\", \"e_meter_ID\":\"" + e_meterID + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryEnergyContractResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		energyContract := new(EnergyContract)
		_ = json.Unmarshal(queryResponse.Value, energyContract)

		queryResult := QueryEnergyContractResult{Key: queryResponse.Key, Record: energyContract}
		results = append(results, queryResult)
	}

	return results, nil
}

// QueryAllActiveEnergyContractsForE_Meter returns all EnergyContracts for a specific E_Meter found in world state
func (s *SmartContract) QueryAllActiveEnergyContractsForE_Meter(ctx contractapi.TransactionContextInterface, e_meterID string, updateTS string) ([]QueryEnergyContractResult, error) {
	objectType := "EnergyContract"

	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\", \"e_meter_ID\":\"" + e_meterID + "\", \"$or\":[{\"endTS\":{\"$gt\":" + updateTS + "},{\"endTS\":null}]}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryEnergyContractResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		energyContract := new(EnergyContract)
		_ = json.Unmarshal(queryResponse.Value, energyContract)

		queryResult := QueryEnergyContractResult{Key: queryResponse.Key, Record: energyContract}
		results = append(results, queryResult)
	}

	return results, nil
}

// QueryAllEnergyContractsForCustomer returns all EnergyContracts for a specific Customer found in world state
func (s *SmartContract) QueryAllEnergyContractsForCustomer(ctx contractapi.TransactionContextInterface, customerID string) ([]QueryEnergyContractResult, error) {
	objectType := "EnergyContract"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\", \"customer_ID\":\"" + customerID + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryEnergyContractResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		energyContract := new(EnergyContract)
		_ = json.Unmarshal(queryResponse.Value, energyContract)

		queryResult := QueryEnergyContractResult{Key: queryResponse.Key, Record: energyContract}
		results = append(results, queryResult)
	}

	return results, nil
}

// CreateRentalContract adds a new RentalContract to the world state with given details
func (s *SmartContract) CreateRentalContract(ctx contractapi.TransactionContextInterface, contractID string, contractorID string, customerID string, startTS string, endTS string, signingTS string, isSigned string, isDiscontinued string, flatID string) error {
	temp, err := s.QueryRentalContract(ctx, contractID)
	if temp != nil && err == nil {
		return fmt.Errorf("RentalContract with this ID already exist.")
	}

	// parse String values
	_isSigned, err := strconv.ParseBool(isSigned)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_isDiscontinued, err := strconv.ParseBool(isDiscontinued)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_startTS, err := strconv.ParseInt(startTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_endTS, err := strconv.ParseInt(endTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_signingTS, err := strconv.ParseInt(signingTS, 10, 64)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	rentalContract := RentalContract{
		ObjectType:     "RentalContract",
		Contractor_ID:  contractorID,
		Customer_ID:    customerID,
		StartTS:        _startTS,
		EndTS:          _endTS,
		SigningTS:      _signingTS,
		IsSigned:       _isSigned,
		IsDiscontinued: _isDiscontinued,
		Flat_ID:        flatID,
		UpdateTS:       time.Now().Unix(),
	}
	rentalContractAsBytes, _ := json.Marshal(rentalContract)

	return ctx.GetStub().PutState(contractID, rentalContractAsBytes)
}

// QueryAllRentalContractsForFlat returns all contracts for a specific flat found in world state
func (s *SmartContract) QueryAllRentalContractsForFlat(ctx contractapi.TransactionContextInterface, flatID string) ([]QueryRentalContractResult, error) {
	objectType := "RentalContract"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\", \"flat_ID\":\"" + flatID + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryRentalContractResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		rentalContract := new(RentalContract)
		_ = json.Unmarshal(queryResponse.Value, rentalContract)

		queryResult := QueryRentalContractResult{Key: queryResponse.Key, Record: rentalContract}
		results = append(results, queryResult)
	}

	return results, nil
}

// QueryRentalContract returns the RentalContracts stored in the world state with given id
func (s *SmartContract) QueryRentalContract(ctx contractapi.TransactionContextInterface, rentalContractID string) (*RentalContract, error) {
	rentalContractAsBytes, err := ctx.GetStub().GetState(rentalContractID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if rentalContractAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", rentalContractID)
	}

	rentalContract := new(RentalContract)
	_ = json.Unmarshal(rentalContractAsBytes, rentalContract)

	return rentalContract, nil
}

// QueryAllRentalContracts returns all RentalContracts found in world state
func (s *SmartContract) QueryAllRentalContacts(ctx contractapi.TransactionContextInterface) ([]QueryRentalContractResult, error) {
	objectType := "RentalContract"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryRentalContractResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		rentalContract := new(RentalContract)
		_ = json.Unmarshal(queryResponse.Value, rentalContract)

		queryResult := QueryRentalContractResult{Key: queryResponse.Key, Record: rentalContract}
		results = append(results, queryResult)
	}

	return results, nil
}
