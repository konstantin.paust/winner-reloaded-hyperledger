module github.com/hyperledger/fabric-samples/chaincode/winner/go

go 1.13

require (
	github.com/hyperledger/fabric-contract-api-go v1.1.0
	github.com/jmoiron/jsonq v0.0.0-20150511023944-e874b168d07e // indirect
)
