package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"strconv"
	"time"
)

type Building struct {
	ObjectType          string `json:"objectType"`
	External_ID         string `json:"external_ID"`
	FlatCount           int    `json:"flatCount"`
	City                string `json:"city"`
	Postcode            string `json:"postcode"`
	Street              string `json:"street"`
	HouseNumber         string `json:"house_number"`
	HasEnergyRessources bool   `json:"has_energy_ressources"`
	UpdateTS            int64  `json:"updateTS"`
}

type Flat struct {
	ObjectType    string  `json:"objectType"`
	Building_ID   string  `json:"building_ID"`
	External_ID   string  `json:"external_ID"`
	AmountOfRooms float32 `json:"amountOfRooms"`
	Size          float32 `json:"size"`
	SizeUnit      string  `json:"sizeUnit"`
	UpdateTS      int64   `json:"updateTS"`
}

// QueryResult structure used for handling result of query
type QueryBuildingResult struct {
	Key    string `json:"Key"`
	Record *Building
}

// QueryResult structure used for handling result of query
type QueryFlatResult struct {
	Key    string `json:"Key"`
	Record *Flat
}

// QueryResult structure used for handling result of history-query
type QueryFlatHistoryResult struct {
	TxId      string `json:"TxId"`
	Key       string `json:"Key"`
	Timestamp int64  `json:"timestamp"`
	IsDelete  string `json:"isDelete"`
	Record    *Flat
}

// InitLedger adds a base set of flats to the ledger
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {
	flats := []Flat{
		Flat{ObjectType: "Flat", External_ID: "ExtFlatID1234", AmountOfRooms: 3, Size: 60, SizeUnit: "m²", UpdateTS: 1612870409, Building_ID: "BUILDING5"},
		Flat{ObjectType: "Flat", External_ID: "ExtFlatID1235", AmountOfRooms: 3, Size: 60, SizeUnit: "m²", UpdateTS: 1612870409, Building_ID: "BUILDING6"},
		Flat{ObjectType: "Flat", External_ID: "ExtFlatID1236", AmountOfRooms: 3, Size: 60, SizeUnit: "m²", UpdateTS: 1612870410, Building_ID: "BUILDING6"},
		Flat{ObjectType: "Flat", External_ID: "ExtFlatID1237", AmountOfRooms: 3, Size: 60, SizeUnit: "m²", UpdateTS: 1612870411, Building_ID: "BUILDING7"},
		Flat{ObjectType: "Flat", External_ID: "ExtFlatID1238", AmountOfRooms: 3, Size: 60, SizeUnit: "m²", UpdateTS: 1612870412, Building_ID: "BUILDING7"},
		Flat{ObjectType: "Flat", External_ID: "ExtFlatID1239", AmountOfRooms: 3, Size: 60, SizeUnit: "m²", UpdateTS: 1612870413, Building_ID: "BUILDING7"},
	}

	for i, flat := range flats {
		flatAsBytes, _ := json.Marshal(flat)
		err := ctx.GetStub().PutState("FLAT"+strconv.Itoa(i+5), flatAsBytes)

		if err != nil {
			return fmt.Errorf("Failed to put to world state. %s", err.Error())
		}
	}

	buildings := []Building{
		Building{ObjectType: "Building", External_ID: "ExtBuildingID1234", Street: "Musterstraße", HouseNumber: "1", Postcode: "07745", City: "Jena", FlatCount: 65, HasEnergyRessources: true, UpdateTS: 1668466800},
		Building{ObjectType: "Building", External_ID: "ExtBuildingID1235", Street: "Musterstraße", HouseNumber: "2", Postcode: "07745", City: "Jena", FlatCount: 66, HasEnergyRessources: true, UpdateTS: 1668466800},
		Building{ObjectType: "Building", External_ID: "ExtBuildingID1236", Street: "Musterstraße", HouseNumber: "3a", Postcode: "07745", City: "Jena", FlatCount: 67, HasEnergyRessources: true, UpdateTS: 1668466800},
		Building{ObjectType: "Building", External_ID: "ExtBuildingID1237", Street: "Musterstraße", HouseNumber: "3b", Postcode: "07745", City: "Jena", FlatCount: 68, HasEnergyRessources: true, UpdateTS: 1668466800},
		Building{ObjectType: "Building", External_ID: "ExtBuildingID1238", Street: "Musterstraße", HouseNumber: "4", Postcode: "07745", City: "Jena", FlatCount: 69, HasEnergyRessources: false, UpdateTS: 1668466800},
		Building{ObjectType: "Building", External_ID: "ExtBuildingID1239", Street: "Musterstraße", HouseNumber: "5", Postcode: "07745", City: "Jena", FlatCount: 70, HasEnergyRessources: false, UpdateTS: 1668466800},
	}

	for i, building := range buildings {
		buildingAsBytes, _ := json.Marshal(building)
		err := ctx.GetStub().PutState("BUILDING"+strconv.Itoa(i+5), buildingAsBytes)

		if err != nil {
			return fmt.Errorf("Failed to put to world state. %s", err.Error())
		}
	}

	e_meters := []E_Meter{
		E_Meter{ObjectType: "E_Meter", Flat_ID: "FLAT5", Owner_ID: "MessBetr1", Meter_value: 1000, Last_meter_value: 900, Diff_meter_value: 100, UpdateTS: 1668466800, Unit: "kWh", IsActive: true},
		E_Meter{ObjectType: "E_Meter", Flat_ID: "FLAT5", Owner_ID: "MessBetr1", Meter_value: 1200, Last_meter_value: 1000, Diff_meter_value: 200, UpdateTS: 1668467800, Unit: "kWh", IsActive: true},
		E_Meter{ObjectType: "E_Meter", Flat_ID: "FLAT6", Owner_ID: "MessBetr1", Meter_value: 1000, Last_meter_value: 1000, Diff_meter_value: 0, UpdateTS: 1668466800, Unit: "kWh", IsActive: true},
		E_Meter{ObjectType: "E_Meter", Flat_ID: "FLAT6", Owner_ID: "MessBetr1", Meter_value: 1300, Last_meter_value: 1200, Diff_meter_value: 100, UpdateTS: 1668468800, Unit: "kWh", IsActive: true},
		E_Meter{ObjectType: "E_Meter", Flat_ID: "", Owner_ID: "MessBetr1", EnergyRessource_ID: "PV123", Meter_value: 1500, Last_meter_value: 1000, Diff_meter_value: 100, UpdateTS: 1668466800, Unit: "kWh", IsActive: true},
		E_Meter{ObjectType: "E_Meter", Flat_ID: "", Owner_ID: "MessBetr1", EnergyRessource_ID: "PV124", Meter_value: 1600, Last_meter_value: 1500, Diff_meter_value: 100, UpdateTS: 1668469800, Unit: "kWh", IsActive: true},
	}

	for i, e_meter := range e_meters {
		e_meterAsBytes, _ := json.Marshal(e_meter)
		err := ctx.GetStub().PutState("E_Meter"+strconv.Itoa(i+5), e_meterAsBytes)

		if err != nil {
			return fmt.Errorf("Failed to put to world state. %s", err.Error())
		}
	}
	return nil
}

// CreateFlat adds a new flat to the world state with given details
func (s *SmartContract) CreateFlat(ctx contractapi.TransactionContextInterface, flatID string, buildingID string, externalID string, amountOfRooms string, size string, sizeUnit string) error {
	temp, err := s.QueryFlat(ctx, flatID)

	if temp != nil && err == nil {
		return fmt.Errorf("Flat with this ID already exist.")
	}
	now := time.Now()

	// parse String values
	_amountOfRooms, err := strconv.ParseFloat(amountOfRooms, 32)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_size, err := strconv.ParseFloat(size, 32)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	flat := Flat{
		ObjectType:    "Flat",
		Building_ID:   buildingID,
		External_ID:   externalID,
		AmountOfRooms: float32(_amountOfRooms),
		Size:          float32(_size),
		SizeUnit:      sizeUnit,
		UpdateTS:      now.Unix(),
	}
	flatAsBytes, _ := json.Marshal(flat)

	return ctx.GetStub().PutState(flatID, flatAsBytes)
}

// UpdateFlat updates an existing flat-object in world state
func (s *SmartContract) UpdateFlat(ctx contractapi.TransactionContextInterface, flatID string, newBuildingID string, newExternalID string, newAmountOfRooms string, newSize string, newSizeUnit string) error {
	flatAsBytes, err := ctx.GetStub().GetState(flatID)
	if err != nil {
		return fmt.Errorf("Failed to read from world state. %s", err.Error())
	}
	flat := new(Flat)
	_ = json.Unmarshal(flatAsBytes, flat)
	now := time.Now()

	// parse String values
	_newAmountOfRooms, err := strconv.ParseFloat(newAmountOfRooms, 32)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_newSize, err := strconv.ParseFloat(newSize, 32)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	flat.Building_ID = newBuildingID
	flat.External_ID = newExternalID
	flat.AmountOfRooms = float32(_newAmountOfRooms)
	flat.Size = float32(_newSize)
	flat.SizeUnit = newSizeUnit
	flat.UpdateTS = now.Unix()

	updatedFlatAsBytes, _ := json.Marshal(flat)

	return ctx.GetStub().PutState(flatID, updatedFlatAsBytes)
}

// CreateBuilding adds a new Building to the world state with given details
func (s *SmartContract) CreateBuilding(ctx contractapi.TransactionContextInterface, buildingID string, externalID string, city string, postcode string, street string, houseNumber string, flatCount string) error {
	temp, err := s.QueryBuilding(ctx, buildingID)

	if temp != nil && err == nil {
		return fmt.Errorf("Building with this ID already exist.")
	}

	// parse String values
	_flatCount, err := strconv.ParseInt(flatCount, 10, 32)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	now := time.Now()
	building := Building{
		ObjectType:  "Building",
		External_ID: externalID,
		Postcode:    postcode,
		City:        city,
		Street:      street,
		HouseNumber: houseNumber,
		FlatCount:   int(_flatCount),
		UpdateTS:    now.Unix(),
	}
	buildingAsBytes, _ := json.Marshal(building)

	return ctx.GetStub().PutState(buildingID, buildingAsBytes)
}

// UpdateBuilding updates an existing building-object in world state
func (s *SmartContract) UpdateBuilding(ctx contractapi.TransactionContextInterface, buildingID string, newExternalID string, newCity string, newPostcode string, newStreet string, newHouseNumber string, newFlatCount string, newHasEnergyRessource string) error {
	buildingAsBytes, err := ctx.GetStub().GetState(buildingID)
	if err != nil {
		return fmt.Errorf("Failed to read from world state. %s", err.Error())
	}
	building := new(Building)
	_ = json.Unmarshal(buildingAsBytes, building)
	now := time.Now()

	// parse String values
	_newHasEnergyRessource, err := strconv.ParseBool(newHasEnergyRessource)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}
	_newFlatCount, err := strconv.ParseInt(newFlatCount, 10, 32)
	if err != nil {
		return fmt.Errorf("Fehler beim Parsen des Strings! Objekt wurde nicht erzeugt")
	}

	building.External_ID = newExternalID
	building.Street = newStreet
	building.HouseNumber = newHouseNumber
	building.Postcode = newPostcode
	building.City = newCity
	building.FlatCount = int(_newFlatCount)
	building.HasEnergyRessources = _newHasEnergyRessource
	building.UpdateTS = now.Unix()

	updatedBuildingAsBytes, _ := json.Marshal(building)

	return ctx.GetStub().PutState(buildingID, updatedBuildingAsBytes)
}

// QueryFlat returns the flat stored in the world state with given id
func (s *SmartContract) QueryFlat(ctx contractapi.TransactionContextInterface, flatID string) (*Flat, error) {
	flatAsBytes, err := ctx.GetStub().GetState(flatID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if flatAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", flatID)
	}

	flat := new(Flat)
	_ = json.Unmarshal(flatAsBytes, flat)

	return flat, nil
}

// QueryAllFlats returns all flats found in world state
func (s *SmartContract) QueryAllFlats(ctx contractapi.TransactionContextInterface) ([]QueryFlatResult, error) {
	objectType := "Flat"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryFlatResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		flat := new(Flat)
		_ = json.Unmarshal(queryResponse.Value, flat)

		queryResult := QueryFlatResult{Key: queryResponse.Key, Record: flat}
		results = append(results, queryResult)
	}

	return results, nil
}

// QueryAllFlatsForBuilding returns all flats for a specific building found in world state
func (s *SmartContract) QueryAllFlatsForBuilding(ctx contractapi.TransactionContextInterface, buildingID string) ([]QueryFlatResult, error) {
	objectType := "Flat"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\", \"building_ID\":\"" + buildingID + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryFlatResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		flat := new(Flat)
		_ = json.Unmarshal(queryResponse.Value, flat)

		queryResult := QueryFlatResult{Key: queryResponse.Key, Record: flat}
		results = append(results, queryResult)
	}

	return results, nil
}

// QueryBuilding returns the buildings stored in the world state with given id
func (s *SmartContract) QueryBuilding(ctx contractapi.TransactionContextInterface, buildingID string) (*Building, error) {
	buildingAsBytes, err := ctx.GetStub().GetState(buildingID)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if buildingAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", buildingID)
	}

	building := new(Building)
	_ = json.Unmarshal(buildingAsBytes, building)

	return building, nil
}

// QueryAllBuildings returns all buildings found in world state
func (s *SmartContract) QueryAllBuildings(ctx contractapi.TransactionContextInterface) ([]QueryBuildingResult, error) {
	objectType := "Building"
	resultsIterator, err := ctx.GetStub().GetQueryResult("{\"selector\":{\"objectType\":\"" + objectType + "\"}}")

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryBuildingResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		building := new(Building)
		_ = json.Unmarshal(queryResponse.Value, building)

		queryResult := QueryBuildingResult{Key: queryResponse.Key, Record: building}
		results = append(results, queryResult)
	}

	return results, nil
}
