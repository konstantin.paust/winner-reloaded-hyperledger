/*
SPDX-License-Identifier: Apache-2.0
*/

package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeoutException;

import org.eclipse.paho.client.mqttv3.*;
import org.hyperledger.fabric.gateway.*;

import org.json.JSONArray;
import org.json.JSONObject;


public class ClientApp {
	private static Path walletPath;
	private static Path networkConfigPath;
	private static IMqttClient client;
	private static Contract winnerContract;
	static {
		System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "false");
	}

	public static void createInvoice(String invoiceID, String conractID, String externalInvoiceID, Long startTS, Long endTS) throws IOException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();


		//Blockchain connection
		System.out.println("Builder");
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		System.out.println("Builder connect");
		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");
			System.out.println("submit");
			contract.submitTransaction("CreateInvoice", invoiceID, conractID, externalInvoiceID, Long.toString(startTS), Long.toString(endTS));
			//subscibeMqtt(client, topic);


		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void createEnergyPrice(String priceID, double price, Long startTS, String owner, double buildingPrice, String currency, String metadata ) throws IOException, ContractException, InterruptedException, TimeoutException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();

		/*try {
			winnerContract.submitTransaction("CreateEnergyPrice", priceID, Double.toString(price), Long.toString(startTS), Long.toString(timeStampSec), owner,Double.toString(buildingPrice),metadata);
		} catch (ContractException | TimeoutException | InterruptedException e){
			e.printStackTrace();
		}*/


		//Blockchain connection
		System.out.println("Builder");
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		System.out.println("Builder connect");
		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");
			System.out.println("submit");
			contract.submitTransaction("CreateEnergyPrice", priceID, Double.toString(price), Long.toString(startTS), Long.toString(timeStampSec), owner,Double.toString(buildingPrice),metadata);
			//subscibeMqtt(client, topic);


		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void updateEnergyPriceValue(String priceID, double price, Long startTS, double buildingPrice, String currency ) throws IOException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();


		//Blockchain connection
		System.out.println("Builder");
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		System.out.println("Builder connect");
		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");
			System.out.println("submit");
			contract.submitTransaction("UpdateEnergyPriceValue", priceID, Double.toString(price), Long.toString(timeStampSec), Long.toString(startTS), Double.toString(buildingPrice));
			//subscibeMqtt(client, topic);


		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static JSONArray queryEnergyPrice(String priceID) throws IOException {

		//Blockchain connection
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);
		JSONArray jsonArray = null;

		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");

			byte[] result = contract.evaluateTransaction("QueryEnergyPrice", priceID);
			JSONObject json = new JSONObject(new String(result)); // Convert text to json object
			System.out.println("\n" + json.toString(4)); // Print it with specified indentation

		} catch (ContractException e) {
			e.printStackTrace();
		}
		return jsonArray;
	}

	public static void createEnergyContract(String contractID, String contractorID, String customerID, Long startTS, Long endTS, Long signingTS, boolean isSigned, boolean isDiscontinued,  String e_meterID, String tariffID ) throws IOException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();


		//Blockchain connection
		System.out.println("Builder");
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		System.out.println("Builder connect");
		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");
			System.out.println("submit");
			contract.submitTransaction("CreateEnergyContract", contractID, contractorID, customerID, Long.toString(startTS), Long.toString(endTS), Long.toString(signingTS), Boolean.toString(isSigned), Boolean.toString(isDiscontinued), e_meterID, tariffID);
			//subscibeMqtt(client, topic);


		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void createTariffModel(String tariffID, String name, String externalID, String priceID, String metadata ) throws IOException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();


		//Blockchain connection
		System.out.println("Builder");
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		System.out.println("Builder connect");
		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");
			System.out.println("submit");
			contract.submitTransaction("CreateTariffModel", tariffID, name, externalID, priceID,metadata);
			//subscibeMqtt(client, topic);


		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void createNewE_Meter(String flatID, String energyMeterID, String meterValue, String owner) throws IOException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();

		//generate topic
		String topic = "winner/" + energyMeterID;

		//Blockchain connection
		System.out.println("Builder");
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		System.out.println("Builder connect");
		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");
			System.out.println("submit");
			contract.submitTransaction("CreateE_Meter", energyMeterID, owner, flatID, "", meterValue, Long.toString(timeStampSec));
			//subscibeMqtt(client, topic);


		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void changeE_meter(String oldE_meter_ID, double oldE_Meter_value, String newE_meter_ID, String newE_Meter_ownerID, double meterValue) throws IOException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();

		//Blockchain connection
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");

			contract.submitTransaction("ChangeE_Meter", oldE_meter_ID, Double.toString(oldE_Meter_value), newE_meter_ID, newE_Meter_ownerID, Double.toString(meterValue), Long.toString(timeStampSec));

		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void invokeChaincode(String message, String topic) throws IOException {

		//timestamp for updateTS
		Instant instant = Instant.now();
		long timeStampSec = instant.getEpochSecond();

		//Blockchain connection
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		//cut topic only to E_MeterID
		String e_meterID = topic.substring(topic.indexOf('/')+1);

		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");

			//Chaincode function updateFlatMeterValue
			contract.submitTransaction("UpdateE_MeterValue", e_meterID, message, Long.toString(timeStampSec));

			//print worldstate after each update for testing
			byte[] result = contract.evaluateTransaction("QueryE_Meter", e_meterID);
			JSONObject json = new JSONObject(new String(result)); // Convert text to json object
			System.out.println("\n" + json.toString(4)); // Print it with specified indentation

		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void updateEmeterValue(String emeterID, Float emeterValue, long timestamp) throws IOException {
		//Blockchain connection
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);

		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");

			//Chaincode function updateFlatMeterValue
			contract.submitTransaction("UpdateE_MeterValue", emeterID, Float.toString(emeterValue), Long.toString(timestamp));

			//print worldstate after each update for testing
			byte[] result = contract.evaluateTransaction("QueryE_Meter", emeterID);
			//JSONObject json = new JSONObject(new String(result)); // Convert text to json object
			//System.out.println("\n" + json.toString(4)); // Print it with specified indentation

		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static JSONObject queryChaincode(String e_meterID) throws IOException {

		//Blockchain connection
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);
		JSONObject json = null;

		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");

			//Chaincode function "QueryFlat" and return json
			byte[] result = contract.evaluateTransaction("QueryE_Meter", e_meterID);
			json = new JSONObject(new String(result)); // Convert text to object

		} catch (ContractException e) {
			e.printStackTrace();
		}
		return json;
	}

	public static JSONArray queryE_MeterHistoryBetweenTS(String e_meterID, long startTS, long endTS) throws IOException {

		//Blockchain connection
		Gateway.Builder builder = Gateway.createBuilder();
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);
		JSONArray json = null;

		try (Gateway gateway = builder.connect()) {

			// get the network and contract
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("winner");

			//Chaincode function "QueryE_MeterHistoryBetweenTS" and return json
			byte[] result = contract.evaluateTransaction("QueryE_MeterBetweenTS", e_meterID, Long.toString(startTS), Long.toString(endTS));
			json = new JSONArray(new String(result)); // Convert text to object

		} catch (ContractException e) {
			e.printStackTrace();
		}
		return json;
	}

	public static IMqttClient setMqtt(String brokerUrl) throws MqttException {
		String publisherId = UUID.randomUUID().toString();
		return new MqttClient(brokerUrl,publisherId);
	}

	public static void connectMqtt(IMqttClient publisher, String user, String pw) throws MqttException {
		MqttConnectOptions options = new MqttConnectOptions();
		options.setAutomaticReconnect(true);
		options.setCleanSession(true);
		options.setConnectionTimeout(120);

		//pw and username for broker
		options.setUserName(user);
		options.setPassword((pw).toCharArray());

		publisher.setCallback(new MqttCallback() {
			public void connectionLost(Throwable cause) {}

			public void messageArrived(String topic, MqttMessage message) {
				//run work in a thread to send back an acknowledge fast enough
				Thread thread = new Thread(() -> {
					try {
						//topic looks like: winner/E_MeterID
						invokeChaincode(message.toString(), topic);
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
				thread.start();
			}
			public void deliveryComplete(IMqttDeliveryToken token) {}
		});
		publisher.connect(options);

	}

	public static void subscibeMqtt(IMqttClient publisher, String topic) throws MqttException {
		publisher.subscribe(topic);
	}

	public static void createRandomPrices(String priceID, int counter, Long startTS, Long timeInbetween  ) throws IOException {
		Random r = new Random();
		for(int i = 0; i<30;i++){
			double createdRanNum = r.nextDouble();
			updateEnergyPriceValue(priceID,createdRanNum,(startTS+(i*startTS)),createdRanNum,"€");
			//queryEnergyPrice(priceID);
		}
	}
	public static void main(String[] args) throws Exception {
		//EnrollAdmin.main(null);
		//RegisterUser.main(null);

		// Load a file system based wallet for managing identities.
		walletPath = Paths.get("wallet");
		Wallet wallet = Wallets.newFileSystemWallet(walletPath);
		// load a CCP

		networkConfigPath = Paths.get("..","peerOrganizations", "org1.winner-testnet.com","connection-org1.yaml");
		//networkConfigPath = Paths.get("..","..","..","BackupNodes","node1", "cpp.yaml");
		System.out.println("before Gateway Builder");
		Gateway.Builder builder = Gateway.createBuilder();
		System.out.println("before Builder Identity");
		builder.identity(wallet, "admin").networkConfig(networkConfigPath).discovery(true);


		System.out.println("before Gateway Builder connect");
		// create a gateway connection
		try (Gateway gateway = builder.connect()) {


			System.out.println("Builder connect ");
			// get the network and contract
			System.out.println("get Network");
			Network network = gateway.getNetwork("mychannel");
			System.out.println("getchannel");
			winnerContract = network.getContract("winner");
			System.out.println("invoke");
			byte[] result;


			client = setMqtt("tcp://paust-seebach-home.spdns.de:8883");
			connectMqtt(client, "Winner", "BAsEE357QkcFway");
			subscibeMqtt(client, "winner/E_Meter10");

			//createNewE_Meter
			//createNewE_Meter("FLAT5","E_Meter25","139","TEAG");

			//create energy price
			//System.out.println("create Prices");
			//createEnergyPrice("MyLowPrice2",0.25, 1621000000L,"Me",0.25,"€","" );
			//createRandomPrices("MyLowPrice2",100,1621000000L,2000L);
			//queryEnergyPrice("MyLowPrice2");

			//System.out.println("create Tariff");
			//createTariffModel("Tariff002","GreenTariff","GreenTariffMore","MyLowPrice2","");

			//System.out.println("create Contract");
			//createEnergyContract("EvilContract2","EvilCorp","CUS001",1620000000L,1650000000L, 1620000000L,true,false,"E_Meter25","Tariff002");

			//Query History for specific key
			//JSONArray json = queryE_Meter("E_Meter10");
			//System.out.println("\n" + json.toString(4));

			//Query world state for all flats
			//result = contract.evaluateTransaction("queryAllFlats");
			//JSONArray json = new JSONArray(new String(result)); // Convert text to object
			//System.out.println("\n" + json.toString(4));
			//System.out.println("Change E_Meter");
			//change E_meter_ID for FLAT10
			////changeE_meter("E_Meter5",140,"E_Meter11", "TEAG", 163);

			//get E_meter_History for timewindow
			System.out.println("QueryHistory");
			Instant instant = Instant.now();
			//JSONArray json = queryE_MeterHistoryBetweenTS("E_Meter10", (int) (instant.getEpochSecond() - 300), (int) instant.getEpochSecond());
   			//System.out.println(queryE_MeterHistoryBetweenTS("E_Meter10", (1668469800 - 300), 1668469800 + 300));
			//System.out.println("\n" + json.toString(4));

			//read csv
			/*String line = "";
			String splitBy = ",";
			try
			{
				//parsing a CSV file into BufferedReader class constructor
				BufferedReader br = new BufferedReader(new FileReader("data.csv"));
				br.readLine();
				while ((line = br.readLine()) != null)   //returns a Boolean value
				{
					String[] eMeter = line.split(splitBy);    // use comma as separator
					System.out.println("eMeter [Name=" + eMeter[0] + ", Time=" + eMeter[1] + ", non_negative_difference_value=" + eMeter[2] + ", first_value=" + eMeter[3] + ", last_value= " + eMeter[4] + "]");
					Instant date = Instant.parse(eMeter[1]);
					updateEmeterValue("E_Meter25",Float.parseFloat(eMeter[4]), date.getEpochSecond());
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
*/
			System.out.println("create Invoice");
			createInvoice("TestInvoice2","EvilContract2","Evilex2",1621023000L,1651023000L);

		}
	}

}
