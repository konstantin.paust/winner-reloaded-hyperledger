#!/usr/bin/env bash

# look for binaries in local dev environment /build/bin directory and then in local samples /bin directory
export PATH="${PWD}"/../fabric-samples/bin:"$PATH"
export FABRIC_CFG_PATH="${PWD}"/../fabric-samples/config

export FABRIC_LOGGING_SPEC=INFO
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_ROOTCERT_FILE="${PWD}"/crypto-config/peerOrganizations/org1.winner-testnet.com/peers/peer1.org1.winner-testnet.com/tls/ca.crt
export CORE_PEER_ADDRESS=129.168.178.254:7053
export CORE_PEER_LOCALMSPID=Org1MSP
export CORE_PEER_MSPCONFIGPATH="${PWD}"/crypto-config/peerOrganizations/org1.winner-testnet.com/users/Admin@org1.winner-testnet.com/msp

# join peer to channel
peer channel join -b "${PWD}"/channel-artifacts/mychannel.block
