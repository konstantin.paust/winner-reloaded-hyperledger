#!/usr/bin/env bash
set -euo pipefail

# look for binaries in local dev environment /build/bin directory and then in local samples /bin directory
export PATH="${PWD}"/../fabric-samples/bin:"$PATH"
export FABRIC_CFG_PATH="${PWD}"/../fabric-samples/config

export FABRIC_LOGGING_SPEC=debug:cauthdsl,policies,msp,grpc,peer.gossip.mcs,gossip,leveldbhelper=info
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_CERT_FILE="${PWD}"/crypto-config/peerOrganizations/org2.winner-testnet.com/peers/peer0.org2.winner-testnet.com/tls/server.crt
export CORE_PEER_TLS_KEY_FILE="${PWD}"/crypto-config/peerOrganizations/org2.winner-testnet.com/peers/peer0.org2.winner-testnet.com/tls/server.key
export CORE_PEER_TLS_ROOTCERT_FILE="${PWD}"/crypto-config/peerOrganizations/org2.winner-testnet.com/peers/peer0.org2.winner-testnet.com/tls/ca.crt
export CORE_PEER_ID=peer0.org2.winner-testnet.com
export CORE_PEER_ADDRESS=192.168.178.253:7061
export CORE_PEER_LISTENADDRESS=0.0.0.0:7061
export CORE_PEER_CHAINCODEADDRESS=host.docker.internal:7062
export CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7062
# bootstrap peer is the other peer in the same org
export CORE_PEER_GOSSIP_BOOTSTRAP=192.168.178.253:7063
export CORE_PEER_GOSSIP_EXTERNALENDPOINT=192.168.178.253:7061
export CORE_PEER_LOCALMSPID=Org2MSP
export CORE_PEER_MSPCONFIGPATH="${PWD}"/crypto-config/peerOrganizations/org2.winner-testnet.com/peers/peer0.org2.winner-testnet.com/msp
export CORE_OPERATIONS_LISTENADDRESS=0.0.0.0:8449
export CORE_PEER_FILESYSTEMPATH="${PWD}"/data/peer0.org2.winner-testnet.com
export CORE_LEDGER_SNAPSHOTS_ROOTDIR="${PWD}"/data/peer0.org2.winner-testnet.com/snapshots

# uncomment the lines below to utilize couchdb state database, when done with the environment you can stop the couchdb container with "docker rm -f couchdb3"
export CORE_LEDGER_STATE_STATEDATABASE=CouchDB
export CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=127.0.0.1:5983
export CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=admin
export CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
docker run --publish 5983:5984 --detach -e COUCHDB_USER=admin -e COUCHDB_PASSWORD=password --name couchdb3 couchdb:3.1.1

# start peer
peer node start
