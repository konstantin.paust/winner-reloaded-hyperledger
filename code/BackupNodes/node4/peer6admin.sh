#!/usr/bin/env bash

# look for binaries in local dev environment /build/bin directory and then in local samples /bin directory
export PATH="${PWD}"/../fabric-samples/bin:"$PATH"
export FABRIC_CFG_PATH="${PWD}"/../fabric-samples/config

export FABRIC_LOGGING_SPEC=INFO
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_ROOTCERT_FILE="${PWD}"/crypto-config/peerOrganizations/org3.winner-testnet.com/peers/peer1.org3.winner-testnet.com/tls/ca.crt
export CORE_PEER_ADDRESS=192.168.178.252:7073
export CORE_PEER_LOCALMSPID=Org3MSP
export CORE_PEER_MSPCONFIGPATH="${PWD}"/crypto-config/peerOrganizations/org3.winner-testnet.com/users/Admin@org3.winner-testnet.com/msp

# join peer to channel
peer channel join -b "${PWD}"/channel-artifacts/mychannel.block
